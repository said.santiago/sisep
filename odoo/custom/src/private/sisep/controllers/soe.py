import logging

from odoo import http
from odoo.http import request

_logger = logging.getLogger(__name__)


class SoeController(http.Controller):
    @http.route(
        ["/sisep/soe/interview/<string:identifier>"], type="json", auth="none", cors="*"
    )
    def accept_soe_data(self, identifier=None):
        token_req = request.httprequest.headers.get("SoeToken")
        ICP = request.env["ir.config_parameter"]
        token_icp = ICP.sudo().get_param("sisep.soe_token")
        if token_req != token_icp:
            return {"status": "unauthorized"}
        json_data = request.jsonrequest
        if not identifier:
            raise Exception

        SI = request.env["soe.interview"]

        si = SI.sudo().search([("guid", "=like", identifier)])
        si.ensure_one()
        si.write(
            {
                "status": json_data.get("status"),
                "observations": json_data.get("observations"),
                "result_attachment": json_data.get("result_attachment"),
            }
        )
        si.application_id.soe_score = json_data.get("score", "0")
        _logger.info("Updated %s with %s" % (si, json_data))

        return {"status": "done"}

    @http.route(
        ["/sisep/soe/interview/getinfo/<string:identifier>"],
        type="json",
        auth="none",
        cors="*",
    )
    def get_info(self, identifier=None):
        token_req = request.httprequest.headers.get("SoeToken")
        ICP = request.env["ir.config_parameter"]
        token_icp = ICP.sudo().get_param("sisep.soe_token")
        if token_req != token_icp:
            return {"status": "unauthorized"}
        if not identifier:
            raise Exception

        SI = request.env["soe.interview"]

        si = SI.sudo().search([("guid", "=like", identifier)])
        try:
            si.ensure_one()
        except Exception:
            dd = {"status": "nodata"}
        else:
            dd = {
                "mail_interviewer": si.mail_interviewer,
                "mail_interviewed": si.mail_interviewed,
                "company_name": si.company_id.name,
                "date_expiry": si.date_expiry.strftime("%Y-%m-%d"),
                "is_code": si.application_id.is_code,
                "application_data": si.application_id.read(
                    [
                        "campaign_id",
                        "source_id",
                        "medium_id",
                        "activity_ids",
                        "activity_state",
                        "activity_user_id",
                        "activity_type_id",
                        "activity_type_icon",
                        "activity_date_deadline",
                        "my_activity_date_deadline",
                        "activity_summary",
                        "activity_exception_decoration",
                        "activity_exception_icon",
                        "activity_calendar_event_id",
                        "email_cc",
                        "name",
                        "active",
                        "description",
                        "email_from",
                        "probability",
                        "partner_id",
                        "create_date",
                        "stage_id",
                        "last_stage_id",
                        "categ_ids",
                        "company_id",
                        "user_id",
                        "date_closed",
                        "date_open",
                        "date_last_stage_update",
                        "priority",
                        "job_id",
                        "salary_proposed_extra",
                        "salary_expected_extra",
                        "salary_proposed",
                        "salary_expected",
                        "availability",
                        "partner_name",
                        "partner_phone",
                        "partner_mobile",
                        "type_id",
                        "department_id",
                        "day_open",
                        "day_close",
                        "delay_close",
                        "color",
                        "emp_id",
                        "user_email",
                        "attachment_number",
                        "employee_name",
                        "attachment_ids",
                        "kanban_state",
                        "legend_blocked",
                        "legend_done",
                        "legend_normal",
                        "application_count",
                        "refuse_reason_id",
                        "meeting_ids",
                        "meeting_display_text",
                        "meeting_display_date",
                        "id",
                        "__last_update",
                        "display_name",
                        "create_uid",
                        "write_uid",
                        "write_date",
                        "survey_id",
                        "response_id",
                        "response_state",
                        "stage_is_soe",
                        "soe_interview_ids",
                        "soe_done",
                        "survey_ids",
                    ]
                ),
            }
        return dd
