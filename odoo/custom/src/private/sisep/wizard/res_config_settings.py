# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models


class ResConfigSettings(models.TransientModel):
    _inherit = "res.config.settings"

    soe_base_url = fields.Char(
        "Soe Domain",
        default="https://sisep-video-chat.netlify.app/",
        config_parameter="sisep.soe_base_url",
        help="The soe external server base url",
    )
    soe_token = fields.Char(
        "Soe Token",
        default="TheTokenForSOEAuthentication",
        config_parameter="sisep.soe_token",
        help="The soe token used for authentication",
    )


class ApplicantGetRefuseReason(models.TransientModel):
    _inherit = "applicant.get.refuse.reason"

    @api.depends("refuse_reason_id")
    def _compute_send_mail(self):
        for wizard in self:
            template = wizard.refuse_reason_id.sudo().template_id
            if not template:
                continue
            template_name = template.with_context(lang="en_US").name
            template = (
                self.env["mail.template"]
                .with_context(lang="en_US")
                .search(
                    [
                        ("name", "ilike", template_name),
                        ("company_id", "=", self.env.company.id),
                    ]
                )
            )
            template.ensure_one()
            wizard.send_mail = bool(template)
            wizard.template_id = template


# class SurveyInvite(models.TransientModel):
#     _inherit = 'survey.invite'
#
#     partner_ids = fields.Many2many(domain=False)
