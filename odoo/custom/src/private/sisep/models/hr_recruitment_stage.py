import logging
import uuid
from datetime import timedelta

from odoo import SUPERUSER_ID, _, api, exceptions, fields, models

# from odoo.osv import expression
from odoo.exceptions import UserError
from odoo.osv import expression

from odoo.addons.auth_signup.models.res_partner import now

_logger = logging.getLogger(__name__)


class HrApplicantApproval(models.Model):
    _name = "hr.applicant.approval"

    user_id = fields.Many2one("res.users", required=False)
    applicant_id = fields.Many2one("hr.applicant", required=True)
    stage_id = fields.Many2one(
        "hr.recruitment.stage",
        required=True,
        help="The stage of the hr.applicant cannot be moved if the approval was not done",
    )
    group_id = fields.Many2one("res.groups")
    approved = fields.Boolean()
    company_id = fields.Many2one(
        "res.company",
        string="Company",
        index=True,
        default=lambda self: self.env.company,
    )
    config_id = fields.Many2one(
        "hr.recruitment.stage.approval.config", ondelete="cascade"
    )

    def approve(self):
        self.approved = True

    def disapprove(self):
        self.approved = False

    def write(self, values):
        if self.env.context.get("ignore_perm_checks"):
            return super().write(values)
        authorized = True
        for haa in self:
            if not (
                self.env.user.id == haa.user_id.id
                or (haa.group_id and self.env.user in haa.group_id.users or False)
            ):
                authorized = False
                break
        if authorized:
            return super().write(values)
        else:
            raise exceptions.ValidationError(
                _("Unable to change approval status because you are not allowed")
            )


class RecruitmentStageApprovalConfig(models.Model):
    _name = "hr.recruitment.stage.approval.config"

    stage_id = fields.Many2one("hr.recruitment.stage", required=True)
    user_id = fields.Many2one("res.users")
    group_id = fields.Many2one("res.groups")
    company_id = fields.Many2one(
        "res.company",
        string="Company",
        index=True,
        default=lambda self: self.env.company,
    )

    @api.model_create_multi
    def create(self, values):
        """ When creating update all the applications with specified configuration """
        HAA = self.env["hr.applicant.approval"]
        HA = self.env["hr.applicant"]
        res = super().create(values)
        approvals_to_create = HA.search([])
        for ha_todo in approvals_to_create:
            for hrsac in res:
                if (
                    hrsac.stage_id.job_ids
                    and ha_todo.job_id.id not in hrsac.stage_id.job_ids.ids
                ):
                    continue
                HAA.create(
                    {
                        "user_id": hrsac.user_id.id,
                        "applicant_id": ha_todo.id,
                        "stage_id": hrsac.stage_id.id,
                        "group_id": hrsac.group_id.id,
                        "config_id": hrsac.id,
                    }
                )
        return res

    def write(self, values):
        """ When writing update all the applications with specified configuration """
        HAA = self.env["hr.applicant.approval"]
        approvals_to_update = HAA.search(
            [
                ("config_id", "=", self.id),
            ]
        )
        res = super().write(values)
        for atu in approvals_to_update:
            atu.with_context(ignore_perm_checks=True).write(
                {
                    "user_id": self.user_id.id,
                    "stage_id": self.stage_id.id,
                    "group_id": self.group_id.id,
                    "company_id": self.company_id.id,
                }
            )
        return res

    # def unlink(self):
    #     """ When deleting update all the applications with specified configuration """
    #     HAA = self.env["hr.applicant.approval"]
    #     approvals_to_delete = HAA.search([
    #         ('config_id', '=', self.id),
    #     ])
    #     res = super().unlink()
    #     approvals_to_delete.unlink()
    #     return res


class RecruitmentStage(models.Model):
    _inherit = "hr.recruitment.stage"

    company_id = fields.Many2one(
        "res.company",
        string="Company",
        index=True,
        default=lambda self: self.env.company,
    )
    is_soe = fields.Boolean(
        "Is SOE?",
        help="By checking this the hr.applicant cannot move from stage if the soe_done boolean is not completed",  # noqa
    )
    approval_config_ids = fields.One2many(
        "hr.recruitment.stage.approval.config", "stage_id", "Approval Configuration"
    )


class SurveyUserInput(models.Model):
    """ Metadata for a set of one user's answers to a particular survey """

    _inherit = "survey.user_input"

    company_id = fields.Many2one(related="survey_id.company_id")
    needs_evaluation = fields.Boolean(compute="_compute_needs_evaluation", store=True)
    applicant_id = fields.Many2one("hr.applicant")
    user_id = fields.Many2one("res.users", default=lambda self: self.env.user)

    @api.depends(
        "user_input_line_ids.answer_score",
        "user_input_line_ids.question_id",
        "predefined_question_ids.answer_score",
    )
    def _compute_scoring_values(self):
        for user_input in self:
            # sum(multi-choice question scores) + sum(simple answer_type scores)
            total_possible_score = 0
            for question in user_input.predefined_question_ids:
                if question.is_scored_by_user_id:
                    total_possible_score += 1
                elif question.question_type == "simple_choice":
                    total_possible_score += max(
                        [
                            score
                            for score in question.mapped(
                                "suggested_answer_ids.answer_score"
                            )
                            if score > 0
                        ],
                        default=0,
                    )
                elif question.question_type == "multiple_choice":
                    total_possible_score += sum(
                        score
                        for score in question.mapped(
                            "suggested_answer_ids.answer_score"
                        )
                        if score > 0
                    )
                elif question.is_scored_question:
                    total_possible_score += question.answer_score

            if total_possible_score == 0:
                user_input.scoring_percentage = 0
                user_input.scoring_total = 0
            else:
                score_total = sum(user_input.user_input_line_ids.mapped("answer_score"))
                user_input.scoring_total = score_total
                score_percentage = (score_total / total_possible_score) * 100
                user_input.scoring_percentage = (
                    round(score_percentage, 2) if score_percentage > 0 else 0
                )

    @api.depends("user_input_line_ids.requires_scoring")
    def _compute_needs_evaluation(self):
        self.needs_evaluation = any(self.mapped("user_input_line_ids.requires_scoring"))

    def create(self, values):
        HA = self.env["hr.applicant"]
        mail = values.get("email")
        if mail:
            ha = HA.search([("email_from", "=ilike", mail)])
            if ha:
                values["applicant_id"] = ha.id
        return super().create(values)

    def mark_user_input_line_ids_resolved(self):
        # self.user_input_line_ids.write({'requires_scoring': False})
        self.env.cr.execute(
            "UPDATE survey_user_input_line SET requires_scoring=False WHERE id IN %(id)s",
            {"id": tuple(self.user_input_line_ids.ids)},
        )
        self._compute_needs_evaluation()


class SurveySurvey(models.Model):
    _inherit = "survey.survey"

    company_id = fields.Many2one(
        "res.company",
        string="Company",
        index=True,
        default=lambda self: self.env.company,
    )

    def copy_data(self, default=None):
        new_defaults = {"title": _("%s") % (self.title)}
        default = dict(new_defaults, **(default or {}))
        return super(SurveySurvey, self).copy_data(default)

    def action_send_survey(self):
        """ Open a window to compose an email, pre-filled with the survey message """
        res = super().action_send_survey()
        mail_tmpl = (
            self.env["mail.template"]
            .with_context(lang="en_US")
            .search([("name", "ilike", "Survey: Invite")])
        )
        res["context"]["default_template_id"] = mail_tmpl.id
        return res

    def write(self, values):
        if "user_id" in values:
            res = super(SurveySurvey, self.sudo()).write(values)
        else:
            res = super(SurveySurvey, self).write(values)
        return res


class SurveyQuestion(models.Model):
    _inherit = "survey.question"

    is_scored_by_user_id = fields.Boolean("Is scored by responsible?")
    company_id = fields.Many2one(related="survey_id.company_id", store=True)

    @api.model
    def create(self, values):
        if values.get("is_scored_by_user_id"):
            values["is_scored_question"] = True
            values["answer_score"] = 1
        return super().create(values)

    def write(self, values):
        is_scored_by_user_id = values.get("is_scored_by_user_id", -10)
        if is_scored_by_user_id is True:
            values["is_scored_question"] = True
            values["answer_score"] = 1
        elif is_scored_by_user_id is False:
            values["is_scored_question"] = False
            values["answer_score"] = 0
        elif is_scored_by_user_id == -10:
            pass
        return super().write(values)


class SurveyUserInputLine(models.Model):
    _inherit = "survey.user_input.line"

    requires_scoring = fields.Boolean(
        string="Requires manual scoring",
        compute="_compute_requires_scoring",
        store=True,
    )
    user_id = fields.Many2one(related="question_id.survey_id.user_id", store=True)
    company_id = fields.Many2one(related="question_id.survey_id.company_id", store=True)

    @api.constrains("answer_score")
    def _check_answer_score_for_scored_by_user_id(self):
        for suil in self:
            if suil.question_id.is_scored_by_user_id and not (
                0 <= suil.answer_score <= 1
            ):
                raise exceptions.ValidationError(_("0 <= %s <= 1") % suil.answer_score)

    @api.depends("question_id.is_scored_by_user_id", "answer_score")
    def _compute_requires_scoring(self):
        for suil in self:
            if suil.question_id.is_scored_by_user_id and not suil.answer_score:
                suil.requires_scoring = True
            else:
                suil.requires_scoring = False

    @api.model
    def _get_answer_score_values(self, vals, compute_speed_score=True):
        question_id = vals.get("question_id")
        if not question_id:
            raise ValueError(_("Computing score requires a question in arguments."))
        question = self.env["survey.question"].browse(int(question_id))

        # default and non-scored questions
        if question.is_scored_by_user_id:
            return {
                "answer_is_correct": False,
                "answer_score": 0,
            }
        return super()._get_answer_score_values(
            vals, compute_speed_score=compute_speed_score
        )


AVAILABLE_PRIORITIES = [
    ("0", "Fail"),
    ("1", "Poor"),
    ("2", "Normal"),
    ("3", "Good"),
    ("4", "Very Good"),
    ("5", "Excellent"),
]


class HrApplicant(models.Model):
    _inherit = "hr.applicant"

    stage_id = fields.Many2one(compute="_compute_stage_patched")
    stage_is_soe = fields.Boolean(related="stage_id.is_soe")
    is_code = fields.Boolean("Is code application?")
    soe_interview_ids = fields.One2many(
        "soe.interview", "application_id", "SOE Interviews"
    )
    soe_done = fields.Boolean(
        "Soe Finished?",
        help="If the stage is soe, this enables to move on to next stage",
    )
    survey_ids = fields.One2many("survey.user_input", "applicant_id", "Surveys")
    approval_ids = fields.One2many(
        "hr.applicant.approval", "applicant_id", "Approvals", track_visibility="always"
    )
    score = fields.Float(compute="_compute_score", store=True, string="Final Score (%)")
    soft_score = fields.Selection(
        AVAILABLE_PRIORITIES, default="0", string="Soft Score"
    )
    hard_score = fields.Selection(
        AVAILABLE_PRIORITIES, default="0", string="Hard Score"
    )
    soe_score = fields.Selection(AVAILABLE_PRIORITIES, default="0", string="SOE Score")
    partner_id = fields.Many2one("res.partner")

    @api.depends("soe_score", "soft_score", "hard_score")
    def _compute_score(self):
        for ha in self:
            # Score of soe
            soe_score = int(ha.soe_score)
            soft_score = int(ha.soft_score)
            hard_score = int(ha.hard_score)
            # Actual Score / 15
            ha.score = ((soe_score + soft_score + hard_score) / 15) * 100

    @api.constrains("soft_score", "hard_score")
    def _check_scoring_boundaries(self):
        if not 0 <= int(self.soft_score) <= 10:
            raise exceptions.ValidationError(
                _("Boundaries error. Minimum is 0, Maximum is 5")
            )
        if not 0 <= int(self.hard_score) <= 10:
            raise exceptions.ValidationError(
                _("Boundaries error. Minimum is 0, Maximum is 5")
            )
        if not 0 <= int(self.soe_score) <= 10:
            raise exceptions.ValidationError(
                _("Boundaries error. Minimum is 0, Maximum is 5")
            )
        return True

    def write(self, values):
        if "email_from" in values:
            self.partner_id.email = values["email_from"]
            self.partner_id.name = values["email_from"]
        if (
            self.env.context.get("params")
            and self.env.context.get("params")["model"] == "hr.applicant"
            and "soe_score" in values
        ):
            raise exceptions.ValidationError(_("You shall not qualify SOE interview"))
        if values.get("stage_id") and self.stage_id.is_soe and not self.soe_done:
            raise exceptions.ValidationError(
                _("Cannot switch to another stage, first the soe must be done")
            )
        # Before moving to another stage, check that all approvals are done
        if values.get("stage_id"):
            self.is_stage_fully_approved()
            self.ensure_stage_non_approved(values.get("stage_id"))
        HRS = self.env["hr.recruitment.stage"]
        # If next stage is soe, set the soe_done in false to enable checking by that boolean
        if values.get("stage_id") and HRS.browse(values.get("stage_id")).is_soe:
            values["soe_done"] = False
        res = super().write(values)
        return res

    def get_start_url(self):
        self.ensure_one()
        SI = self.env["soe.interview"]
        si_drafts = SI.search(
            [("application_id", "=", self.id), ("status", "=", "draft")]
        )
        si_drafts.write(
            {"status": "error", "observations": _("Interview closed by a new one")}
        )
        d_expiry = fields.Date.context_today(self) + timedelta(7)
        soe_interview_vals = {
            "date_expiry": d_expiry,
            "mail_interviewer": self.sudo().user_id.partner_id.email,
            "mail_interviewed": self.email_from,
            "application_id": self.id,
        }
        si = SI.create(soe_interview_vals)
        return si.complete_url

    def action_send_soe_survey(self):
        """ Generates a link soe.interview and embed it on a mail to send interviewed """
        self.ensure_one()
        # Send the mail

        self.ensure_one()
        template_id = (
            self.env["mail.template"]
            .with_context(lang="en_US")
            .search([("name", "ilike", "Interview: Invite")])
            .id
        )
        ctx = {
            "default_model": "hr.applicant",
            "default_res_id": self.ids[0],
            "default_use_template": bool(template_id),
            "default_template_id": template_id,
            "default_composition_mode": "comment",
            "custom_layout": "mail.mail_notification_paynow",
            "proforma": self.env.context.get("proforma", False),
            "force_email": True,
        }
        return {
            "type": "ir.actions.act_window",
            "view_mode": "form",
            "res_model": "mail.compose.message",
            "views": [(False, "form")],
            "view_id": False,
            "target": "new",
            "context": ctx,
        }

    @api.depends("job_id")
    def _compute_stage_patched(self):
        for applicant in self:
            if applicant.job_id:
                if not applicant.stage_id:
                    stage_ids = (
                        self.env["hr.recruitment.stage"]
                        .search(
                            [
                                "|",
                                ("job_ids", "=", False),
                                ("job_ids", "=", applicant.job_id.id),
                                ("fold", "=", False),
                                ("company_id", "=", self.env.company.id),
                            ],
                            order="sequence asc",
                            limit=1,
                        )
                        .ids
                    )
                    applicant.stage_id = stage_ids[0] if stage_ids else False
            else:
                applicant.stage_id = False

    @api.model
    def create(self, values):
        res = super().create(values)
        res.setup_approvals(values)
        email = values.get("email_from")
        RP = self.env["res.partner"]
        if email:
            pid = RP.search([("email", "=ilike", email)])
            if pid:
                _logger.info("Partner for mail %s found: %s" % (email, pid))
            else:
                rpid = RP.create(
                    {
                        "name": email,
                        "email": email,
                    }
                )
                res.partner_id = rpid.id
        return res

    def setup_approvals(self, values):
        stage_ids = (
            self.env["hr.recruitment.stage"]
            .search(
                [
                    "|",
                    ("job_ids", "=", False),
                    ("job_ids", "=", self.job_id.id),
                    ("fold", "=", False),
                    ("company_id", "=", self.env.company.id),
                ],
                order="sequence asc",
            )
            .ids
        )
        HRSAC = self.env["hr.recruitment.stage.approval.config"]
        HAA = self.env["hr.applicant.approval"]
        approvals_to_create = HRSAC.search([("stage_id", "in", stage_ids)])
        for app in approvals_to_create:
            HAA.create(
                {
                    "user_id": app.user_id.id,
                    "applicant_id": self.id,
                    "stage_id": app.stage_id.id,
                    "group_id": app.group_id.id,
                    "config_id": app.id,
                }
            )

    def is_stage_fully_approved(self):
        HAA = self.env["hr.applicant.approval"]
        approvals = HAA.search(
            [("stage_id", "=", self.stage_id.id), ("applicant_id", "=", self.id)]
        )
        if not approvals:
            return True
        approvals_state = approvals.mapped("approved")
        if not all(approvals_state):
            raise exceptions.ValidationError(
                _("Not all approvals are fullfilled, please review them.")
            )

    def ensure_stage_non_approved(self, next_stage):
        """ If re-entering in an approved stage, write the approvals to false """
        HAA = self.env["hr.applicant.approval"]
        approvals = HAA.search(
            [("stage_id", "=", next_stage), ("applicant_id", "=", self.id)]
        )
        if approvals:
            approvals.sudo().with_context(ignore_perm_checks=True).write(
                {"approved": False}
            )


class SoeInterview(models.Model):
    _name = "soe.interview"

    @api.model
    def _default_uid(self):
        return uuid.uuid4().hex

    guid = fields.Char("UID", default=_default_uid)
    complete_url = fields.Char(compute="_compute_url")
    status = fields.Selection(
        [
            ("draft", "Draft"),
            ("in_progress", "In Progress"),
            ("done", "Done"),
            ("error", "Error"),
        ],
        default=lambda x: "draft",
    )
    observations = fields.Text()
    result_attachment = fields.Char()
    date_expiry = fields.Date()
    mail_interviewer = fields.Char()
    mail_interviewed = fields.Char()
    company_id = fields.Many2one(
        "res.company", string="Company", default=lambda self: self.env.company
    )
    application_id = fields.Many2one("hr.applicant", required=True)
    informed_to_soe = fields.Boolean("SOE Notified?", default=True)
    score = fields.Integer()

    @api.depends("guid")
    def _compute_url(self):
        base_url, token = self.get_soe_config()
        for interview in self:
            interview.complete_url = "%s?guid=%s&token=%s" % (
                base_url,
                interview.guid,
                token,
            )

    def write(self, values):
        status = values.get("status")
        if status == "done":
            self.application_id.soe_done = True
        return super().write(values)

    def create(self, values):
        """ When creating interview inform to soe relevant data """
        res = super().create(values)
        res.inform_to_soe()
        return res

    def inform_to_soe(self):
        """ Send GUID, DATE_EXPIRY, MAIL_INTERVIEWER, MAIL_INTERVIEWED """
        _logger.info("Deprecated: SOE Now asks for information")
        # data_to_send = {
        #     'guid': self.guid,
        #     'date_expiry': self.date_expiry,
        #     'mail_interviewer': self.mail_interviewer,
        #     'mail_interviewed': self.mail_interviewed,
        # }
        # ICP = self.env['ir.config_parameter'].sudo()
        # soe_endpoint = ICP.get_param('soe_endpoint', default='https://soe.sisep.com.ar')
        # try:
        #     requests.post(soe_endpoint, data=data_to_send)
        # except Exception:
        #     _logger.exception('Unable to inform data to SOE')
        #     self.informed_to_soe = False
        # else:
        #     self.informed_to_soe = True

    def gather_data_from_soe(self):
        """
        Send GUID and receives
        {'status': xxx, 'observations': xxx, 'result_attachment': xxx}
        """
        raise NotImplementedError

    def get_soe_config(self):
        ICP = self.env["ir.config_parameter"].sudo()
        base_url = ICP.get_param("sisep.soe_base_url")
        token = ICP.get_param("sisep.soe_token")
        return base_url, token

    def action_goto_soe_interview(self):
        action = {
            "res_model": "soe.interview",
            "res_id": self.id,
            "type": "ir.actions.act_window",
            "name": _("Interview %s") % self.guid,
            "view_mode": "form",
            "views": [[self.env.ref("sisep.view_soe_interview_form").id, "form"]],
        }
        return action


class HrEmployeeSkillHistory(models.Model):
    _name = "hr.employee.skill.history"

    employee_skill_id = fields.Many2one(
        "hr.employee.skill", required=True, ondelete="cascade"
    )
    level_progress = fields.Integer(required=True)


class HrEmployeeSkill(models.Model):
    _inherit = "hr.employee.skill"

    history_ids = fields.One2many("hr.employee.skill.history", "employee_skill_id")
    company_id = fields.Many2one(
        "res.company",
        default=lambda self: self.env.company,
        ondelete="set null",
    )

    @api.model_create_multi
    def create(self, vals_list):
        """ Save the progress in a history table """
        res = super(HrEmployeeSkill, self).create(vals_list)
        HESH = self.env["hr.employee.skill.history"]
        for hes in res:
            HESH.create(
                {"employee_skill_id": hes.id, "level_progress": hes.level_progress}
            )
        return res

    def write(self, values):
        """ Save the progress in a history table """
        res = super().write(values)
        HESH = self.env["hr.employee.skill.history"]
        for record in self:
            HESH.create(
                {
                    "employee_skill_id": record.id,
                    "level_progress": record.level_progress,
                }
            )
        return res


class HrEmployee(models.Model):
    _inherit = "hr.employee"

    @api.model
    def create(self, values):
        RU = self.env["res.users"]
        mail = values.get("work_email")
        mail_private = values.get("private_email")
        mail_ahi = self.env["res.partner"].browse(values.get("address_home_id")).email
        mail_to_search = mail or mail_private or mail_ahi
        user = values.get("user_id")
        if mail_to_search and not user:
            ru_mail = RU.search([("login", "=like", mail_to_search)])
            if ru_mail.ids:
                values.update(user_id=ru_mail.id)
        new_employee = super(HrEmployee, self).create(values)
        return new_employee


class HrJob(models.Model):
    _inherit = "hr.job"

    def action_scoring_needed(self):
        """ Returns a tree view of any survey that needs scoring """
        SUI = self.env["survey.user_input"]
        survey_id = self.survey_id.id
        domain = [
            ("survey_id", "=", survey_id),
        ]
        suis_ids = SUI.search(domain).ids
        action = {
            "res_model": "survey.user_input",
            "type": "ir.actions.act_window",
            "name": _("Surveys that need revision"),
            "domain": [("id", "in", suis_ids)],
            "view_mode": "tree,form",
            "context": self.env.context,
        }
        return action


class CalendarAttendee(models.Model):
    _inherit = "calendar.attendee"

    def _send_mail_to_attendees(self, mail_template, force_send=False):
        """Send mail for event invitation to event attendees.
        :param mail_template: a mail.template record
        :param force_send: if set to True,
            the mail(s) will be sent immediately (instead of the next queue processing)
        """
        if isinstance(mail_template, str):
            raise ValueError(
                "Template should be a template record, not an XML ID anymore."
            )
        name_of_template = mail_template.sudo().with_context(lang="en_US").name
        template_id = (
            self.env["mail.template"]
            .with_context(lang="en_US")
            .search([("name", "ilike", name_of_template)])
        )
        template_id.ensure_one()
        for attendee in self:
            event = attendee.event_id
            if event.is_soe and not event.videocall_location:
                event.videocall_location = event.applicant_id.get_start_url()
                event.description = _(
                    "To join the interview use the following link: %s"
                    % event.videocall_location
                )
        return super()._send_mail_to_attendees(template_id, force_send=force_send)


class CalendarEvent(models.Model):
    _inherit = "calendar.event"

    is_soe = fields.Boolean("Is SOE Meet?")
    applicant_id = fields.Many2one(
        "hr.applicant", domain="[('stage_id.is_soe', '=', True)]"
    )

    @api.onchange("applicant_id")
    def onchange_applicant(self):
        if self.applicant_id:
            self.partner_ids |= self.applicant_id.partner_id

    def action_sendmail(self):
        if self.is_soe:
            self.videocall_location = self.applicant_id.get_start_url()
            self.description = _(
                "To join the interview use the following link: %s"
                % self.videocall_location
            )
        return super().action_sendmail()

    def action_open_composer(self):
        res = super().action_open_composer()
        template_id = (
            self.env["mail.template"]
            .with_context(lang="en_US")
            .search([("name", "ilike", "Calendar: Event Update")])
            .id
        )
        res["context"]["default_template_id"] = template_id
        return res


class ResPartner(models.Model):
    _inherit = "res.partner"

    company_id = fields.Many2one(default=lambda self: self.env.company)

    @api.model
    def _name_search(
        self, name, args=None, operator="ilike", limit=100, name_get_uid=None
    ):
        """ Hide companies, partners of users, and partners of others companies """
        res = super(ResPartner, self)._name_search(
            name, args, operator=operator, limit=limit, name_get_uid=name_get_uid
        )
        rps = self.env["res.partner"].sudo().browse(res)
        disable_user_filter = self.env.context.get("disable_user_filter")
        rps_fil = rps.filtered(
            lambda x: x.company_id.id == self.env.company.id
            and (bool(x.user_ids) is False if not disable_user_filter else True)
            and x.is_company is False
        )
        return rps_fil.ids

    def _compute_meeting(self):
        return super(
            ResPartner,
            self.with_context(
                disable_user_filter=True,
                disable_company_filter=True,
                disable_only_person_filter=True,
            ),
        )._compute_meeting()

    @api.model
    def _search(
        self,
        args,
        offset=0,
        limit=None,
        order=None,
        count=False,
        access_rights_uid=None,
    ):
        """
        Only see res_partner of logged company
        """

        disable_company_filter = self.env.context.get("disable_company_filter")
        if not disable_company_filter:
            args = expression.AND([[("company_id", "=", self.env.company.id)], args])

        disable_user_filter = self.env.context.get("disable_user_filter")
        if not disable_user_filter:
            args = expression.AND([[("user_ids", "=", False)], args])

        disable_only_person_filter = self.env.context.get("disable_only_person_filter")
        if not disable_only_person_filter:
            args = expression.AND([[("is_company", "=", False)], args])

        res = super()._search(
            args,
            offset=offset,
            limit=limit,
            order=order,
            count=count,
            access_rights_uid=access_rights_uid,
        )
        return res

    @api.model
    def _signup_retrieve_partner(
        self, token, check_validity=False, raise_exception=False
    ):
        res = super(
            ResPartner,
            self.with_context(disable_company_filter=True, disable_user_filter=True),
        )._signup_retrieve_partner(
            token, check_validity=check_validity, raise_exception=raise_exception
        )
        return res


class ResCompany(models.Model):
    _inherit = "res.company"

    @api.model
    def _search(
        self,
        args,
        offset=0,
        limit=None,
        order=None,
        count=False,
        access_rights_uid=None,
    ):
        """
        Only see res_company of logged company
        """
        if self.env.user.id != SUPERUSER_ID:
            args = expression.AND([[("id", "in", self.env.user.company_ids.ids)], args])

        res = super()._search(
            args,
            offset=offset,
            limit=limit,
            order=order,
            count=count,
            access_rights_uid=access_rights_uid,
        )
        return res

    @api.model
    def create(self, values):
        rc = super().create(values)
        rc.setup_sisep_company()
        # Change the company of the partner being created along with the company
        rc.partner_id.company_id = rc.id
        return rc

    def setup_sisep_company(self):
        bc = self.env.ref("base.main_company")
        self = self.with_context(lang="en_US")
        # Copy stages
        HRS = self.env["hr.recruitment.stage"]
        hrs = HRS.sudo().search([("company_id", "=", bc.id)])
        [hr.copy({"company_id": self.id}) for hr in hrs]
        # Copy survey.survey
        SS = self.env["survey.survey"]
        ss = SS.sudo().search([("company_id", "=", bc.id)])
        [ssi.copy({"company_id": self.id, "user_id": False}) for ssi in ss]
        # Copy mail.template
        MT = self.env["mail.template"]
        mts = MT.sudo().search([("company_id", "=", bc.id)])
        [mt.copy({"company_id": self.id}) for mt in mts]
        # Copy hr.applicant.category
        HAC = self.env["hr.applicant.category"]
        hacs = HAC.sudo().search([("company_id", "=", bc.id)])
        [hac.copy({"company_id": self.id}) for hac in hacs]
        # Copy hr.applicant.refuse.reason
        HARR = self.env["hr.applicant.refuse.reason"]
        harrs = HARR.sudo().search([("company_id", "=", bc.id)])
        [harr.copy({"company_id": self.id}) for harr in harrs]


class MailTemplate(models.Model):
    _inherit = "mail.template"

    company_id = fields.Many2one(
        "res.company",
        default=lambda self: self.env.company,
        ondelete="set null",
    )

    @api.returns("self", lambda value: value.id)
    def copy(self, default=None):
        res = super(MailTemplate, self).copy(default=default)
        res.name = self.name
        return res


class HrApplicantCategory(models.Model):
    _inherit = "hr.applicant.category"

    company_id = fields.Many2one(
        "res.company",
        default=lambda self: self.env.company,
        ondelete="set null",
    )
    _sql_constraints = [
        ("name_uniq", "unique (name,company_id)", "Tag name already exists !"),
    ]


class HrApplicantRefuseReason(models.Model):
    _inherit = "hr.applicant.refuse.reason"

    company_id = fields.Many2one(
        "res.company",
        default=lambda self: self.env.company,
        ondelete="set null",
    )

    @api.returns("self", lambda value: value.id)
    def copy(self, default=None):
        self.ensure_one()
        mt_name = self.with_context(lang="en_US").template_id.name
        default = dict(default or {})
        if "template_id" not in default:
            company_id = default["company_id"]
            mt = (
                self.env["mail.template"]
                .with_context(lang="en_US")
                .search([("name", "ilike", mt_name), ("company_id", "=", company_id)])
            )
            default["template_id"] = mt.id
        return super().copy(default=default)


class SkillLevel(models.Model):
    _inherit = "hr.skill.level"

    company_id = fields.Many2one(
        "res.company",
        default=lambda self: self.env.company,
        ondelete="set null",
    )


class SkillType(models.Model):
    _inherit = "hr.skill.type"

    company_id = fields.Many2one(
        "res.company",
        default=lambda self: self.env.company,
        ondelete="set null",
    )


class Skill(models.Model):
    _inherit = "hr.skill"

    company_id = fields.Many2one(
        "res.company",
        default=lambda self: self.env.company,
        ondelete="set null",
    )


class ResPartnerCategory(models.Model):
    _inherit = "res.partner.category"

    company_id = fields.Many2one(
        "res.company",
        default=lambda self: self.env.company,
        ondelete="set null",
    )


class ResPartnerTitle(models.Model):
    _inherit = "res.partner.title"
    # base.model_res_partner_title

    company_id = fields.Many2one(
        "res.company",
        default=lambda self: self.env.company,
        ondelete="set null",
    )


class ResPartnerIndustry(models.Model):
    _inherit = "res.partner.industry"
    # base.model_res_partner_industry

    company_id = fields.Many2one(
        "res.company",
        default=lambda self: self.env.company,
        ondelete="set null",
    )


class ResBank(models.Model):
    _inherit = "res.bank"
    # base.model_res_bank

    company_id = fields.Many2one(
        "res.company",
        default=lambda self: self.env.company,
        ondelete="set null",
    )


class ResPartnerBank(models.Model):
    _inherit = "res.partner.bank"
    # base.model_res_partner_bank

    company_id = fields.Many2one(
        "res.company",
        default=lambda self: self.env.company,
        ondelete="set null",
    )


class HrEmployeeCategory(models.Model):
    _inherit = "hr.employee.category"
    # hr.model_hr_employee_category

    company_id = fields.Many2one(
        "res.company",
        default=lambda self: self.env.company,
        ondelete="set null",
    )


class HrWorkLocation(models.Model):
    _inherit = "hr.work.location"

    # hr.model_hr_work_location
    company_id = fields.Many2one(
        "res.company",
        default=lambda self: self.env.company,
        ondelete="cascade",
    )


class HrDepartureReason(models.Model):
    _inherit = "hr.departure.reason"
    # hr.model_hr_departure_reason
    company_id = fields.Many2one(
        "res.company",
        default=lambda self: self.env.company,
        ondelete="set null",
    )


class HrPlanActivityType(models.Model):
    _inherit = "hr.plan.activity.type"
    # hr.model_hr_plan_activity_type
    company_id = fields.Many2one(
        "res.company",
        default=lambda self: self.env.company,
        ondelete="set null",
    )


class HrPlan(models.Model):
    _inherit = "hr.plan"
    # hr.model_hr_plan
    company_id = fields.Many2one(
        "res.company",
        default=lambda self: self.env.company,
        ondelete="set null",
    )


class ResUsers(models.Model):
    _inherit = "res.users"

    def action_reset_password(self):
        """ create signup token for each user, and send their signup url by email """
        _logger.info("Overriden native action_reset_password")
        if self.env.context.get("install_mode", False):
            return
        if self.filtered(lambda user: not user.active):
            raise UserError(_("You cannot perform this action on an archived user."))
        # prepare reset password signup
        create_mode = bool(self.env.context.get("create_user"))

        # no time limit for initial invitation, only for reset password
        expiration = False if create_mode else now(days=+1)

        self.mapped("partner_id").signup_prepare(
            signup_type="reset", expiration=expiration
        )

        # send email to users with their signup url
        template = False
        PT = self.env["mail.template"]
        if create_mode:
            try:
                template = PT.with_context(lang="en_US").search(
                    [
                        ("name", "=like", "Auth Signup: Odoo Connection"),
                        ("company_id", "=", self.env.company.id),
                    ]
                )
            except ValueError:
                pass
        if not template:
            template = PT.with_context(lang="en_US").search(
                [
                    ("name", "=like", "Auth Signup: Reset Password"),
                    ("company_id", "=", self.env.company.id),
                ]
            )
        assert template._name == "mail.template"

        email_values = {
            "email_cc": False,
            "auto_delete": True,
            "recipient_ids": [],
            "partner_ids": [],
            "scheduled_date": False,
        }

        for user in self:
            if not user.email:
                raise UserError(
                    _("Cannot send email: user %s has no email address.", user.name)
                )
            email_values["email_to"] = user.email
            # TDE FIXME: make this template technical (qweb)
            with self.env.cr.savepoint():
                force_send = not (self.env.context.get("import_file", False))
                template.send_mail(
                    user.id,
                    force_send=force_send,
                    raise_exception=True,
                    email_values=email_values,
                )
            _logger.info(
                "Password reset email sent for user <%s> to <%s>",
                user.login,
                user.email,
            )


class IrUiMenu(models.Model):
    _inherit = "ir.ui.menu"

    @api.model
    @api.returns("self")
    def get_user_roots(self):
        """ Only show full adjustment menues to real `admin` """
        res = super().get_user_roots()
        if self.env.user != self.env.ref("base.user_admin"):
            tech_adjustment = self.env.ref("base.menu_administration")
            res -= tech_adjustment
        return res
